# Fuelder

## Technical documentation

### Description of the application

This is the v1 of the Fuelder application, it is an android application allowing its users to have the price of gasoline from different gas stations in France.

### Functional description of the application

A user can :

 - create an account
 - log in
 - forget password
 - access the home page with his information
 - access the map with his geolocation
 - log out

### User management

Use of firebase for all user management  
Use of firestore to store user data

### Gas station data

Using an XML file downloaded from: https://transport.data.gouv.fr/datasets/prix-des-carburants-en-france-flux-instantane/   
Path: Fuelder/app/src/amin/assets/GasStation.xml

### Permissions

Path: Fuelder/app/src/main/AndroidManifest.xml  

``` 
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```

When the user goes to the map, the application asks for permission for "Location"  
