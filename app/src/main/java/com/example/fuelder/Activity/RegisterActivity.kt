package com.example.fuelder.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.fuelder.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

class RegisterActivity : AppCompatActivity() {

    private lateinit var email: EditText
    private lateinit var firstname: EditText
    private lateinit var lastname: EditText
    private lateinit var password: EditText
    private lateinit var confirm: EditText
    private lateinit var  fAuth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        email = findViewById(R.id.register_email)
        firstname = findViewById(R.id.register_firstname)
        lastname = findViewById(R.id.register_lastname)
        password = findViewById(R.id.register_password)
        confirm = findViewById(R.id.register_confirm)

        fAuth = Firebase.auth

        findViewById<Button>(R.id.btn_register).setOnClickListener{
            validateEmptyForm()
        }


        val buttonClick = findViewById<Button>(R.id.btn_login)
        buttonClick.setOnClickListener {
            val intent = Intent(this, LoginActivity:: class.java )
            startActivity(intent)
        }
    }

    private fun validateEmptyForm() {
        when {
            TextUtils.isEmpty(firstname.text.toString().trim{it <= ' '})->{
                firstname.setError("Veuillez saisir votre Email")
            }
            TextUtils.isEmpty(lastname.text.toString().trim{it <= ' '})->{
                lastname.setError("Veuillez saisir votre Prénom")
            }
            TextUtils.isEmpty(email.text.toString().trim{it <= ' '})->{
                email.setError("Veuillez saisir votre email")
            }
            TextUtils.isEmpty(password.text.toString().trim{it <= ' '})->{
                password.setError("Veuillez saisir votre mot de passe")
            }
            TextUtils.isEmpty(confirm.text.toString().trim{it <= ' '})->{
                confirm.setError("Veuillez confirmer votre mot de passe")
            }

            firstname.text.toString().isNotEmpty() &&
                    lastname.text.toString().isNotEmpty() &&
                    email.text.toString().isNotEmpty() &&
                    password.text.toString().isNotEmpty() &&
                    confirm.text.toString().isNotEmpty() -> {

                        if(email.text.toString().matches(Regex("^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,3}\$"))){
                            if(password.text.toString().length>=5){
                                if(password.text.toString() == confirm.text.toString()){
                                    signUp()
                                }else{
                                    confirm.setError("Les mots de passe ne sont pas identiques")
                                }
                            }else{
                                password.setError("Le mot de passe doit contenir au moins 5 caractères")
                            }
                        }else{
                            email.setError("Veillez saisir une adresse mail valide")
                        }
                    }
        }
    }

    private fun signUp() {

        val email: String = email.text.toString().trim{it <= ' '}
        val firstname: String = firstname.text.toString().trim{it <= ' '}
        val lastname: String = lastname.text.toString().trim{it <= ' '}
        val password: String = password.text.toString().trim{it <= ' '}

        fAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener{
                task ->
                if(task.isSuccessful){
                    val firebaseUser: FirebaseUser = task.result!!.user!!

                    Toast.makeText(
                        this@RegisterActivity,
                        "Vous vous êtes inscrit avec succès.",
                        Toast.LENGTH_SHORT
                    ).show()

                    saveFireStore(firstname, lastname, email)

                    val intent =
                        Intent(this@RegisterActivity, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    intent.putExtra("user_id", firebaseUser.uid)
                    intent.putExtra("email_id", email)
                    startActivity(intent)
                    finish()
                }else{
                    Toast.makeText(this@RegisterActivity,
                        task.exception!!.message, Toast.LENGTH_SHORT).show()
                }
        }

    }

        private fun saveFireStore(firstname: String, lastname: String, email: String){
            val db = FirebaseFirestore.getInstance()
            val user: MutableMap<String, Any> = HashMap()

            user["firstName"] = firstname
            user["lastName"] = lastname
            user["email"] = email

            db.collection("users")
                .add(user)
        }

}