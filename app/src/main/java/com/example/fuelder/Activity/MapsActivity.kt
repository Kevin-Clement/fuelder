package com.example.fuelder.Activity

import android.content.pm.PackageManager
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.example.fuelder.Class.GasStation
import com.example.fuelder.MarkerInfoWindowAdapter
import com.example.fuelder.Class.PlaceRenderer
import com.example.fuelder.R

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng

import com.example.fuelder.databinding.ActivityMapsBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.clustering.ClusterManager
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.InputStream

class MapsActivity : AppCompatActivity(), GoogleMap.OnMarkerClickListener{

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private lateinit var lastLocation: Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var lng: Double = 0.0
    private var lat: Double = 0.0
    private lateinit var latlng: LatLng
    private var city: String = ""
    private var gazole: String = ""
    private var e10: String = ""
    private var sp98: String = ""
    private var gasStations = arrayListOf<GasStation>()


    companion object {
        private  const val LOCATION_REQUEST_CODE = 1
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

     binding = ActivityMapsBinding.inflate(layoutInflater)
     setContentView(binding.root)

        xmlParserData()

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync{
            googleMap: GoogleMap -> addClusteredMarkers(googleMap)
            googleMap.setInfoWindowAdapter(MarkerInfoWindowAdapter(this))
            mMap = googleMap
            setUpMap()
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }


    private fun setUpMap() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            println("TRTR")
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST_CODE)
            return
        }
        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->

            if(location != null){
                lastLocation = location
                val currentLatLong = LatLng(location.latitude, location.longitude)
                // placeMarkerOnMap(currentLatLong)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 12f))
            }
        }
    }

    private fun xmlParserData(){

        var xmlData: InputStream = assets.open("GasStation.xml")
        var factory: XmlPullParserFactory = XmlPullParserFactory.newInstance()
        var parser: XmlPullParser = factory.newPullParser()

        parser.setInput(xmlData, null)

        var event = parser.eventType

        while (event!= XmlPullParser.END_DOCUMENT){
            when (event) {
                XmlPullParser.START_TAG -> {
                    when {
                        parser.getName().equals("pdv") -> {
                            lat = parser.getAttributeValue(null, "latitude").toDouble()/100000
                            lng = parser.getAttributeValue(null, "longitude").toDouble()/100000
                            latlng = LatLng(lat, lng)

                        }
                        parser.getName().equals("ville") -> {
                            city = parser.nextText()
                        }
                        parser.getName().equals("prix") -> {
                            when {
                                parser.getAttributeValue(null, "id") == "1" -> {
                                    gazole = parser.getAttributeValue(null, "valeur")
                                }
                                parser.getAttributeValue(null, "id") == "5" -> {
                                    e10 = parser.getAttributeValue(null, "valeur")
                                }
                                parser.getAttributeValue(null, "id") == "6" -> {
                                    sp98 = parser.getAttributeValue(null, "valeur")
                                }
                            }
                        }
                    }
                }
                XmlPullParser.END_TAG -> {
                   if(parser.getName().equals("pdv")){
                       gasStations.add(GasStation(city, latlng, gazole, e10, sp98))
                   }
                }
                XmlPullParser.TEXT -> {
                }
            }
            event = parser.next()
        }


    }

    /**
     * Adds markers to the map with clustering support.
     */
    private fun addClusteredMarkers(googleMap: GoogleMap) {
        // Create the ClusterManager class and set the custom renderer.
        val clusterManager = ClusterManager<GasStation>(this, googleMap)
        clusterManager.renderer =
            PlaceRenderer(
                this,
                googleMap,
                clusterManager
            )

        // Set custom info window adapter
        clusterManager.markerCollection.setInfoWindowAdapter(MarkerInfoWindowAdapter(this))

        // Add the places to the ClusterManager.
        clusterManager.addItems(gasStations)
        clusterManager.cluster()

        // Set ClusterManager as the OnCameraIdleListener so that it
        // can re-cluster when zooming in and out.
        googleMap.setOnCameraIdleListener {
            clusterManager.onCameraIdle()
        }
    }

    override fun onMarkerClick(p0: Marker) = false
}