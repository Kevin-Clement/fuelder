package com.example.fuelder.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.fuelder.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LoginActivity : AppCompatActivity() {

    private lateinit var email: EditText
    private lateinit var password: EditText
    private lateinit var tvForgotPassword: TextView
    private lateinit var fAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        email = findViewById(R.id.login_email)
        password = findViewById(R.id.login_password)
        tvForgotPassword = findViewById(R.id.tv_forgot_password)
        fAuth = Firebase.auth

        findViewById<Button>(R.id.btn_login).setOnClickListener {
            validateForm()
        }


        val buttonClickRegister = findViewById<Button>(R.id.btn_register)
        buttonClickRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        tvForgotPassword.setOnClickListener{
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        }
    }

    private fun validateForm() {
        when {
            TextUtils.isEmpty(email.text.toString().trim{it <= ' '}) -> {
                email.setError("Veuillez saisir votre email")
            }
            TextUtils.isEmpty(password.text.toString().trim{it <= ' '}) -> {
                password.setError("Veuillez saisir votre mot de passe")
            }


            email.text.toString().isNotEmpty() &&
                    password.text.toString().isNotEmpty() -> {

                if(email.text.toString().matches(Regex("^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,3}\$"))){
                    signIn()
                }else{
                    email.setError("Veuillez saisir une adresse mail valide")
                }
            }
        }
    }

    private fun signIn() {
        val email: String = email.text.toString().trim{it <= ' '}
        val password: String = password.text.toString().trim{it <= ' '}

        fAuth.signInWithEmailAndPassword(email,
            password).addOnCompleteListener{
                task->
                if(task.isSuccessful){
                    val firebaseUser: FirebaseUser = task.result!!.user!!
                    val intent =
                        Intent(this@LoginActivity, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    intent.putExtra("user_id", firebaseUser.uid)
                    intent.putExtra("email_id", email)
                    startActivity(intent)
                    finish()
                }else{
                    Toast.makeText(baseContext, task.exception?.message, Toast.LENGTH_SHORT).show()
                }
        }
    }
}