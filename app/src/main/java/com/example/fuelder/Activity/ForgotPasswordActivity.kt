package com.example.fuelder.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.fuelder.R
import com.google.firebase.auth.FirebaseAuth

class ForgotPasswordActivity : AppCompatActivity() {

    private lateinit var etEmail: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

       etEmail = findViewById(R.id.et_forgot_password_email)

        findViewById<Button>(R.id.btn_reset_password).setOnClickListener{
            resetPassword()
        }
    }

    private fun resetPassword(){
        val email: String = etEmail.text.toString().trim{ it <= ' '}

        if(email.isEmpty()){
            Toast.makeText(
                this,
                "Veuillez renseigner votre adresse email",
                Toast.LENGTH_SHORT
            ).show()
        }else{
            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        Toast.makeText(
                            this,
                            "Un email a été envoyé à l'email saisie",
                            Toast.LENGTH_LONG
                        ).show()

                        finish()
                    } else {
                        Toast.makeText(
                            this,
                            task.exception!!.message.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
        }
    }
}