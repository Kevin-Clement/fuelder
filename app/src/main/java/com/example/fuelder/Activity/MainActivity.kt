package com.example.fuelder.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.fuelder.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {

    private lateinit var fAuth: FirebaseAuth
    private lateinit var email: TextView
    private lateinit var user : TextView
    private lateinit var tvResult: TextView
    var currentUSer: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        fAuth = Firebase.auth
        email = findViewById(R.id.tv_email_id)
        user = findViewById(R.id.tv_user_id)
        tvResult = findViewById(R.id.tv_result)


        currentUSer = fAuth.currentUser

        if(currentUSer != null){
            loadProfile()
        }

        val btnMap = findViewById<Button>(R.id.btn_map)
        btnMap.setOnClickListener {
            val intent = Intent(this, MapsActivity:: class.java )
            startActivity(intent)
        }

        val btnLogout = findViewById<Button>(R.id.btn_logout)
        btnLogout.setOnClickListener {
            Firebase.auth.signOut()
            val intent = Intent(this, LoginActivity:: class.java )
            startActivity(intent)
        }
    }


    private fun loadProfile() {
        val emailId = intent.getStringExtra("email_id")
        val db = FirebaseFirestore.getInstance()
        db.collection("users")
            .get()
            .addOnCompleteListener {
                val result: StringBuffer = StringBuffer()
                if (it.isSuccessful) {
                    email.text = "Adresse email : $emailId"
                    for (document in it.result!!) {
                        if(document.data.getValue("email").toString() == emailId)
                        result.append(document.data.getValue("firstName")).append(" ")
                            .append(document.data.getValue("lastName")).append("\n\n ")
                    }
                    tvResult.setText(result)
                }
            }
    }


}