package com.example.fuelder.Class

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class GasStation(var city: String,var latLng: LatLng, var gazole: String, var e10: String, var sp98: String) : ClusterItem {
    override fun getPosition(): LatLng =
        latLng

    override fun getTitle(): String =
        city

    override fun getSnippet(): String =
        gazole
}