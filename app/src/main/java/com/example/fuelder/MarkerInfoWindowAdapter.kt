package com.example.fuelder
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.example.fuelder.Class.GasStation
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class MarkerInfoWindowAdapter(
    private val context: Context
) : GoogleMap.InfoWindowAdapter {
    override fun getInfoContents(p0: Marker): View? {
        // 1. Get tag
        val place = p0?.tag as? GasStation ?: return null

        // 2. Inflate view and set title, address, and rating
        val view = LayoutInflater.from(context).inflate(
            R.layout.marker_info_contents, null
        )
        view.findViewById<TextView>(
            R.id.text_view_title
        ).text = place.city
        view.findViewById<TextView>(
            R.id.text_view_gazole
        ).text = "Gazole : ${place.gazole} €"
        view.findViewById<TextView>(
            R.id.text_view_e10
        ).text = "E 10 : ${place.e10} €"
        view.findViewById<TextView>(
            R.id.text_view_sp98
        ).text = "SP 98 : ${place.sp98} €"

        return view
    }

    override fun getInfoWindow(p0: Marker): View? {
        // Return null to indicate that the
        // default window (white bubble) should be used
        return null
    }
}